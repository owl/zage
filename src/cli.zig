const std = @import("std");
const dt = @import("zig-datetime");
const clap = @import("clap");
const format = @import("format.zig");
const builtin = @import("builtin");

const util = @import("util.zig");
const bech32 = @import("bech32.zig");
const x25519 = @import("x25519.zig");
const zage = @import("zage.zig");
const stream = @import("stream.zig");

const ChaCha = std.crypto.aead.chacha_poly.ChaCha20Poly1305;
const Hkdf = std.crypto.kdf.hkdf.HkdfSha256;
const HmacSha256 = std.crypto.auth.hmac.sha2.HmacSha256;

const is_debug = builtin.mode == .Debug;

const params = clap.parseParamsComptime(
    \\ -e, --encrypt                    Encrypt the input to the output (default).
    \\ -d, --decrypt                    Decrypt the input to the output.
    \\ -o, --output <path>              Write the result to the file at <path>.
    \\ -a, --armor                      Encrypt to a PEM-encoded format.
    \\ -p, --passphrase                 Encrypt with a passphrase.
    \\ -r, --recipient <recipient>...   Encrypt to the specified <recipient>. Can be repeated.
    \\ -R, --recipients-file <path>...  Encrypt to recipients listed in the file at <recipients-path>. Can be repeated.
    \\ -i, --identity <path>...         Use the identity in file at <path>. Can be repeated.
    \\ -h, --help                       Show this help text
    \\ <path>                           Input file. If not given, input is taken from stdin.
);

pub fn recipient(in: []const u8) error{InvalidRecipient}!zage.AgeRecipient {
    if (in.len != zage.recipient_bytes_len)
        return error.InvalidRecipient;

    return in[0..zage.recipient_bytes_len].*;
}

const parsers = .{
    .path = clap.parsers.string,
    .recipient = recipient,
};

pub fn main() void {
    // 4K of args should be enough for anyone?
    var mem: [4 * 1024]u8 = undefined;
    var gpa = if (is_debug)
        std.heap.GeneralPurposeAllocator(.{}){}
    else
        std.heap.FixedBufferAllocator.init(&mem);
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    // Don't need to free anything before exiting.
    defer if (is_debug) {
        arena.deinit();
        _ = gpa.deinit();
    } else std.crypto.utils.secureZero(u8, &mem);
    const allocator = arena.allocator();

    if (isKeygenExe(allocator))
        return @import("keygen.zig").generate(allocator) catch |e|
            util.reportErrorAndExit("failed to generate key", e);

    const res = clap.parse(
        clap.Help,
        &params,
        parsers,
        // Has its own arena.
        .{ .allocator = gpa.allocator() },
    ) catch |e|
        util.reportErrorAndExit("failed to parse arguments", e);
    defer res.deinit();

    if (res.args.help != 0)
        util.printHelpAndExit(clap, &params, "zage");

    // Open the provided input file or stdin.
    const input_file = blk: {
        for (res.positionals) |path|
            break :blk std.fs.cwd().openFile(path, .{}) catch |e|
                util.reportErrorAndExit("could not open input file", e);

        break :blk std.io.getStdIn();
    };

    // Open the provided output file or stdout.
    const output_file = if (res.args.output) |path|
        std.fs.cwd().createFile(path, .{
            .truncate = false,
            .exclusive = true,
        }) catch |e|
            util.reportErrorAndExit("failed to create output file", e)
    else
        std.io.getStdOut();

    // Decrypt
    if (res.args.decrypt != 0) {
        // TODO: Check incompatible args.
        const id_path = if (0 != res.args.identity.len)
            res.args.identity[0]
        else
            util.reportErrorAndExit("no identity file given", null);
        const identity = format.readIdentityFromPath(id_path) catch |e|
            util.reportErrorAndExit("could not read identity from path", e);

        _ = zage.decrypt(
            allocator,
            identity,
            input_file,
            output_file,
        ) catch |e|
            util.reportErrorAndExit("failed to decrypt input", e);
    } else {
        // Encrypt
        if (res.args.recipient.len == 0 and res.args.@"recipients-file".len == 0)
            util.reportErrorAndExit("missing recipients", null);
        // TODO: Check incompatible args.
        // Keep recipients in a set to avoid duplicates.
        var all_recipients = zage.RecipientSet.init(allocator);

        for (res.args.@"recipients-file") |f| {
            readIdentityFile(f, &all_recipients) catch |e|
                util.reportErrorAndExit("failed to read identity file", e);
        }

        for (res.args.recipient) |r| {
            const xr = x25519.Recipient.fromAgeRecipient(r) catch |e|
                util.reportErrorAndExit("failed to decode recipient", e);
            all_recipients.append(xr) catch |e|
                util.reportErrorAndExit("out of memory", e);
        }

        //var it = all_recipients.iterator();
        //while (it.next()) |r| std.log.warn("recipient: {s}", .{r.key_ptr});

        zage.encrypt(
            all_recipients,
            input_file,
            output_file,
        ) catch |e|
            util.reportErrorAndExit("failed to encrypt input", e);
    }
}

fn isKeygenExe(allocator: std.mem.Allocator) bool {
    if (builtin.os.tag == .windows) {
        var it = try std.process.ArgIterator.initWithAllocator(allocator);
        defer it.deinit();

        if (it.next()) |argv0|
            return std.mem.endsWith(u8, argv0, "n");

        return false;
    } else return std.mem.endsWith(u8, std.mem.span(std.os.argv[0]), "n");
}

fn readIdentityFile(
    path: []const u8,
    recipients: *zage.RecipientSet,
) !void {
    const file = try std.fs.cwd().openFile(path, .{});
    var buf: [zage.recipient_bytes_len + 1]u8 = undefined;

    while (try file.reader().readUntilDelimiterOrEof(&buf, 0x0A)) |line| {
        if (line.len != zage.recipient_bytes_len)
            return error.InvalidRecipient;

        const xr = x25519.Recipient.fromAgeRecipient(line[0..zage.recipient_bytes_len].*) catch
            return error.InvalidRecipient;

        try recipients.append(xr);
    }
}
