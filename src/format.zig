// Implements the age file format.
//
const std = @import("std");
const builtin = @import("builtin");
const zage = @import("zage.zig");
const util = @import("util.zig");
const ChaCha = std.crypto.aead.chacha_poly.ChaCha20Poly1305;

const testing = std.testing;

pub const version_line = "age-encryption.org/v1";
pub const stanza_arrow = "->";
pub const mac_delim = "---";

pub const encoded_mac_length = 43;
pub const encoded_ephemeral_secret_length = 43;
pub const encoded_body_length = 43;
pub const chunk_length = 64 * 1024;
pub const encoded_chunk_length = chunk_length + ChaCha.tag_length;
pub const EncodedChunk = [encoded_chunk_length]u8;
pub const Tag = [ChaCha.tag_length]u8;
pub const Nonce = [ChaCha.nonce_length]u8;
pub const Key = [ChaCha.key_length]u8;

pub const Result = packed struct {
    offset: u32,
    len: u16,
    kind: Kind,

    pub const Kind = enum(u3) {
        version,
        stanza,
        type,
        argument,
        body_line,
        body_line_final,
        mac,
    };

    pub fn sourceSlice(self: @This(), buffer: []const u8) []const u8 {
        std.debug.assert(self.offset + self.len <= buffer.len);
        return buffer[self.offset .. self.offset + self.len];
    }
};

const mem = std.mem;

const BadHackyDebugType = enum {
    X25519,
    scrypt,
    empty,
    stanza,
    @"!\"#$%&'",
    PQRSTUVW,
    grease,
};
const Type = if (builtin.mode == .Debug)
    BadHackyDebugType
else
    enum {
        X25519,
        scrypt,
    };

pub fn HeaderReader(comptime ReaderType: type) type {
    return struct {
        reader: ReaderType,
        buffer: []u8,
        bytes_read: u32 = 0,

        pub const HeaderError = error{
            InvalidVersion,
            InvalidStanza,
            InvalidType,
            InvalidBody,
            InvalidMac,
        };
        pub const Error = HeaderError ||
            ReaderType.Error ||
            error{ EndOfStream, StreamTooLong };
        pub const Pos = enum(u1) { other, last };
        const Self = @This();

        pub fn init(reader: ReaderType, buffer: []u8) Self {
            return .{ .reader = reader, .buffer = buffer };
        }

        pub fn readVersion(self: *Self) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const n = try self.reader.read(buffer[0 .. version_line.len + 1]);
            defer self.bytes_read += @intCast(n);

            if (!(mem.eql(u8, buffer[0 .. n - 1], version_line)) and buffer[n] == 0x0A)
                return HeaderError.InvalidVersion;

            return Result{
                .kind = .version,
                .offset = self.bytes_read,
                .len = @intCast(n - 1),
            };
        }

        pub fn readStanza(self: *Self) !Result {
            const buffer = self.buffer[self.bytes_read..];
            const n = try self.reader.read(buffer[0 .. stanza_arrow.len + 1]);
            defer self.bytes_read += @intCast(n);

            if (!(buffer[n - 1] == 0x20 and mem.eql(u8, buffer[0 .. n - 1], stanza_arrow))) {
                return HeaderError.InvalidStanza;
            }

            return Result{
                .kind = .stanza,
                .offset = self.bytes_read,
                .len = @intCast(n - 1),
            };
        }

        pub fn readType(self: *Self) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const bs = try self.reader.readUntilDelimiter(buffer, 0x20);
            defer self.bytes_read += @intCast(bs.len + 1);

            _ = std.meta.stringToEnum(Type, buffer[0..bs.len]) orelse
                return HeaderError.InvalidType;

            return Result{
                .kind = .type,
                .offset = self.bytes_read,
                .len = @intCast(bs.len),
            };
        }

        pub fn readArgument(self: *Self, pos: Pos) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const del: u8 = if (pos == .last) 0x0A else 0x20;
            const bs = try self.reader.readUntilDelimiter(buffer, del);
            defer self.bytes_read += @intCast(bs.len + 1);

            return Result{
                .kind = .argument,
                .offset = self.bytes_read,
                .len = @intCast(bs.len),
            };
        }

        pub fn readBodyLine(self: *Self) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const bs = try self.reader.readUntilDelimiter(buffer[0 .. 64 + 1], 0x0A);
            defer self.bytes_read += @intCast(bs.len + 1);

            return Result{
                .kind = if (bs.len == 64) .body_line else .body_line_final,
                .offset = self.bytes_read,
                .len = @intCast(bs.len),
            };
        }

        pub fn readMac(self: *Self) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const n = try self.reader.read(buffer[0 .. mac_delim.len + 1]);
            defer self.bytes_read += @intCast(n);

            if (!(mem.eql(u8, buffer[0 .. n - 1], mac_delim)) and buffer[n] == 0x20)
                return HeaderError.InvalidMac;

            const bs = try self.reader.readUntilDelimiter(buffer[mac_delim.len + 1 ..], 0x0A);
            defer self.bytes_read += @intCast(bs.len + 1);

            if (bs.len != encoded_mac_length)
                return HeaderError.InvalidMac;

            return Result{
                .kind = .mac,
                .offset = self.bytes_read + @as(u32, @intCast(n)),
                // Does not include the prefix.
                .len = encoded_mac_length,
            };
        }

        pub fn readStanzaOrMac(self: *Self) Error!Result {
            const buffer = self.buffer[self.bytes_read..];
            const n = try self.reader.read(buffer[0 .. stanza_arrow.len + 1]);
            defer self.bytes_read += @intCast(n);

            if (!mem.eql(u8, buffer[0 .. n - 1], stanza_arrow)) {
                // Try to read mac instead.
                const bs = try self.reader.readUntilDelimiter(buffer[n..], 0x0A);
                defer self.bytes_read += @intCast(bs.len + 1);

                if (bs.len != encoded_mac_length + 1 or !mem.eql(u8, buffer[0..mac_delim.len], mac_delim)) {
                    // std.log.warn("len: {d} {s}", .{ bs.len, buffer[0..mac_delim.len] });
                    return HeaderError.InvalidMac;
                }

                return Result{
                    .kind = .mac,
                    .offset = self.bytes_read + 4,
                    .len = @intCast(bs.len - 1),
                };
            }

            return Result{
                .kind = .stanza,
                .offset = self.bytes_read,
                .len = @intCast(n - 1),
            };
        }
    };
}

pub fn headerReader(
    reader: anytype,
    buffer: []u8,
) HeaderReader(@TypeOf(reader)) {
    return HeaderReader(@TypeOf(reader)).init(reader, buffer);
}

const example =
    \\age-encryption.org/v1
    \\-> X25519 XEl0dJ6y3C7KZkgmgWUicg63EyXJiwBJW8PdYJ/cYBE
    \\qRS0AMjdjPvZ/WT08U2KL4G+PIooA3hy38SvLpvaC1E
    \\--- HK2NmOBN9Dpq0Gw6xMCuhFcQlQLvZ/wQUi/2scLG75s
    \\
;

test "header reader" {
    var fbs = std.io.fixedBufferStream(example);
    const reader = fbs.reader();
    var buf: [encoded_chunk_length]u8 = undefined;
    var hr = HeaderReader(@TypeOf(reader)).init(reader, &buf);

    const h = try hr.readVersion();
    try testing.expectEqualStrings(version_line, h.sourceSlice(example));

    const st = try hr.readStanza();
    try testing.expectEqualStrings(stanza_arrow, st.sourceSlice(example));

    const t = try hr.readType();
    try testing.expectEqualStrings("X25519", t.sourceSlice(example));

    const a = try hr.readArgument(.last);
    try testing.expectEqualStrings(
        "XEl0dJ6y3C7KZkgmgWUicg63EyXJiwBJW8PdYJ/cYBE",
        a.sourceSlice(&buf),
    );

    const b = try hr.readBodyLine();
    try testing.expectEqualStrings(
        "qRS0AMjdjPvZ/WT08U2KL4G+PIooA3hy38SvLpvaC1E",
        b.sourceSlice(&buf),
    );

    const borm = try hr.readStanzaOrMac();
    try testing.expectEqual(Result.Kind.mac, borm.kind);
    try testing.expectEqualStrings(
        "HK2NmOBN9Dpq0Gw6xMCuhFcQlQLvZ/wQUi/2scLG75s",
        borm.sourceSlice(&buf),
    );

    try testing.expectEqual(example.len, hr.bytes_read);

    const mac_fodder = example[0 .. borm.offset - 1];

    try testing.expectStringEndsWith(mac_fodder, mac_delim);
}

pub fn readIdentityFromPath(path: []const u8) error{ReadIdentityError}![zage.identity_bytes_len]u8 {
    var file = std.fs.cwd().openFile(path, .{}) catch
        return error.ReadIdentityError;
    defer file.close();
    return readIdentityFromFile(file);
}

pub fn readIdentityFromFile(file: std.fs.File) error{ReadIdentityError}![zage.identity_bytes_len]u8 {
    var id: [zage.identity_bytes_len + 1]u8 = undefined;

    var reader = file.reader();

    while (true) {
        const bs = reader.readUntilDelimiter(&id, 0x0A) catch
            return error.ReadIdentityError;

        if (!mem.startsWith(u8, bs, zage.secret_key_prefix ++ zage.secret_key_version))
            continue;

        return id[0..zage.identity_bytes_len].*;
    }
}
