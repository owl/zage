const std = @import("std");
const bech32 = @import("bech32.zig");
const X25519 = std.crypto.dh.X25519;
const zage = @import("zage.zig");
const format = @import("format.zig");
const stream = @import("stream.zig");

const ChaCha = std.crypto.aead.chacha_poly.ChaCha20Poly1305;
const Hkdf = std.crypto.kdf.hkdf.HkdfSha256;

pub const SecretKey = [X25519.secret_length]u8;
pub const PublicKey = [X25519.public_length]u8;

pub const Identity = struct {
    secret_key: SecretKey,
    public_key: PublicKey,

    pub const FromSecretError = error{
        MalformedSecretKey,
    };
    pub const UnwrapError = error{
        InvalidData,
        ZeroSharedSecret,
    };
    const Self = @This();

    pub fn fromAgeIdentity(bech32_secret_key: zage.AgeIdentity) FromSecretError!Identity {
        const Decoder = bech32.standard_uppercase.Decoder;
        var buf: zage.AgeIdentity = undefined;
        const result = Decoder.decode(&buf, &bech32_secret_key) catch
            return FromSecretError.MalformedSecretKey;

        if (!std.mem.eql(u8, result.hrp, zage.secret_key_prefix))
            return FromSecretError.MalformedSecretKey;

        const secret_key = result.data[0..X25519.secret_length].*;

        return Identity{
            .secret_key = secret_key,
            .public_key = X25519.recoverPublicKey(secret_key) catch
                return FromSecretError.MalformedSecretKey,
        };
    }

    pub fn unwrap(self: Self, ephemeral: []const u8, body: []const u8) UnwrapError!?zage.FileKey {
        if (format.encoded_ephemeral_secret_length != ephemeral.len or
            format.encoded_body_length != body.len)
            return UnwrapError.InvalidData;

        const b64 = std.base64;
        var b64_dec = b64.Base64Decoder.init(b64.standard_alphabet_chars, null);

        var pk_buf: PublicKey = undefined;
        b64_dec.decode(&pk_buf, ephemeral) catch return UnwrapError.InvalidData;

        var shared_secret = X25519.scalarmult(self.secret_key[0..32].*, pk_buf) catch
            return UnwrapError.InvalidData;

        if (std.mem.allEqual(u8, &shared_secret, 0x00))
            return UnwrapError.ZeroSharedSecret;

        var salt_buf: zage.Salt = undefined;

        salt_buf[0..32].* = pk_buf;
        salt_buf[pk_buf.len..].* = self.public_key;

        const prk = Hkdf.extract(&salt_buf, &shared_secret);
        var wrapping_key_buf: [ChaCha.key_length]u8 = undefined;
        Hkdf.expand(
            &wrapping_key_buf,
            "age-encryption.org/v1/X25519",
            prk,
        );

        var enc_file_key: zage.EncryptedFileKey = undefined;

        b64_dec.decode(&enc_file_key, body) catch return UnwrapError.InvalidData;

        std.debug.assert(enc_file_key.len == @sizeOf(zage.EncryptedFileKey));

        const pnonce: [ChaCha.nonce_length]u8 = [_]u8{0x00} ** 12;
        var file_key: zage.FileKey = undefined;

        _ = stream.aeadDecrypt(
            &file_key,
            &enc_file_key,
            pnonce,
            wrapping_key_buf,
        ) catch
        // There's only AuthenticationError
            return null;

        return file_key;
    }
};

pub const Recipient = struct {
    public_key: PublicKey,

    pub const WrapError = error{};
    pub const FromPublicError = error{
        MalformedPublicKey,
    };

    pub fn wrap(recipient: Recipient, file_key: zage.FileKey) WrapError!void {
        var ephemeral: [X25519.seed_length]u8 = undefined;
        std.crypto.random.bytes(&ephemeral);
        const our_public_key = X25519.scalarmult(
            ephemeral,
            X25519.Curve.basePoint,
        ) catch unreachable;
        const shared_secret = X25519.scalarmult(
            ephemeral,
            recipient.public_key,
        ) catch unreachable;
        const salt = our_public_key ++ recipient.public_key;
        const prk = Hkdf.extract(&salt, &shared_secret);
        var wrapping_key: [ChaCha.key_length]u8 = undefined;
        Hkdf.expand(&wrapping_key, "age-encryption.org/v1/X25519", prk);
        const wrapped_key = stream.aeadEncrypt(wrapping_key, file_key);
        _ = wrapped_key; // autofix
    }

    pub fn fromAgeRecipient(bech32_public_key: zage.AgeRecipient) FromPublicError!Recipient {
        var recip: Recipient = undefined;
        const Decoder = bech32.standard.Decoder;
        var buf: [74]u8 = undefined;
        const result = Decoder.decode(&buf, &bech32_public_key) catch
            return FromPublicError.MalformedPublicKey;

        if (!std.mem.eql(u8, result.hrp, zage.public_key_prefix))
            return FromPublicError.MalformedPublicKey;

        recip.public_key = result.data[0..X25519.public_length].*;

        return recip;
    }
};

const t = std.testing;

test "X25519 from bech32 secret key" {
    const sk = "AGE-SECRET-KEY-1GZFUEUQP57QY7MKJ7VR28HNFDHPEYC22HNLAJ00Y2CJNPSWVWAXSS4KNVL";
    // const pk = "age1hjjnskvdlayfhstj36rk3h5mh3cjlwd73zjlv0jehpfn5n6x2fps4p58qc";

    _ = try Identity.fromAgeIdentity(sk.*);
}
