const std = @import("std");
const clap = @import("clap");
const zage = @import("zage.zig");
const dt = @import("zig-datetime");
const builtin = @import("builtin");
const format = @import("format.zig");
const util = @import("util.zig");

const params = clap.parseParamsComptime(
    \\ -o, --output <path>  Write the result to the file at <path>.
    \\ -y, --convert        Convert an identity file to a recipients file.
    \\ -h, --help           Show this help text.
    \\ <path>               Input file. If not given, input is taken from stdin.
);

const parsers = .{
    .path = clap.parsers.string,
};

pub fn generate(allocator: std.mem.Allocator) !void {
    var identity_buf: [zage.identity_bytes_len]u8 = undefined;
    var recipient_buf: [zage.recipient_bytes_len]u8 = undefined;
    var date_buf: [35]u8 = undefined;

    const res = try clap.parse(
        clap.Help,
        &params,
        parsers,
        .{ .allocator = allocator },
    );
    if (res.args.help != 0)
        util.printHelpAndExit(clap, &params, "zage-keygen");

    const out_file = if (res.args.output) |path|
        try std.fs.cwd().createFile(path, .{
            .truncate = false,
            .exclusive = true,
            .mode = if (@bitSizeOf(std.fs.File.Mode) == 0) 0 else 0o600,
        })
    else
        std.io.getStdOut();
    var bw = std.io.bufferedWriter(out_file.writer());
    defer bw.flush() catch unreachable;
    const writer = bw.writer();

    if (res.args.convert != 0) {
        const id_file = if (0 < res.positionals.len)
            try std.fs.cwd().openFile(res.positionals[0], .{})
        else
            std.io.getStdIn();
        const identity = try format.readIdentityFromFile(id_file);
        const recipient = try zage.recipientFromIdentity(
            &recipient_buf,
            &identity,
        );

        writer.print("{s}\n", .{recipient}) catch unreachable;
        return;
    }

    _ = zage.generateIdentity(&identity_buf);
    const recip = try zage.recipientFromIdentity(
        &recipient_buf,
        &identity_buf,
    );

    const now = dt.datetime.Datetime.now().formatISO8601Buf(&date_buf, false) catch
        unreachable;

    try writer.print(
        \\# created: {s}
        \\# public key: {s}
        \\{s}
        \\
    , .{
        now,
        recip,
        &identity_buf,
    });

    if (!out_file.isTty()) {
        var ebw = util.bufferedStdErr();
        defer ebw.flush() catch unreachable;
        defer ebw.writer().print("public key: {s}\n", .{recip}) catch unreachable;

        const stat = out_file.stat() catch return;

        if (stat.kind == .file and @bitSizeOf(@TypeOf(stat.mode)) != 0 and stat.mode & 0o004 != 0)
            ebw.writer().writeAll("warning: writing secret key to a world-readable file\n") catch unreachable;
    }
}
