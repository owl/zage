const std = @import("std");

// Helpers for cajoling errors in and out of C/wasm
pub fn EnumFromErrorSet(
    comptime ErrSet: type,
    comptime first_err: ErrSet,
) type {
    const eti = std.meta.fields(ErrSet);
    var enumFields: [eti.len]std.builtin.Type.EnumField = undefined;

    inline for (eti, &enumFields) |errf, *enumf| {
        enumf.name = errf.name;
        enumf.value = enumValueFromError(
            ErrSet,
            @intFromError(first_err),
            errf.name,
        );
    }

    return @Type(.{
        .Enum = .{
            .tag_type = std.meta.Int(.unsigned, @bitSizeOf(ErrSet)),
            .fields = &enumFields,
            .decls = &.{},
            .is_exhaustive = false,
        },
    });
}

// I don't know if this makes sense, we'll see. :)
pub fn enumValueFromError(
    comptime ErrSet: type,
    comptime first_idx: comptime_int,
    comptime name: []const u8,
) comptime_int {
    inline for (std.meta.fields(ErrSet), first_idx..) |errf, i| {
        if (comptime std.mem.eql(u8, errf.name, name)) {
            return i;
        }
    }

    unreachable;
}

const TestError = error{ Apa, Midi, Hatt };

const TestErrorAsEnum = EnumFromErrorSet(
    TestError,
    TestError.Apa,
);

test "enum from err" {
    const E = TestError;
    inline for (.{
        E.Apa,
        E.Midi,
        E.Hatt,
    }) |err| {
        const ei: comptime_int = @intFromError(err);
        const en: TestErrorAsEnum = @enumFromInt(ei);
        try std.testing.expectEqualStrings(@tagName(en), @errorName(err));
    }
}

pub fn bufferedStdErr() std.io.BufferedWriter(4096, std.fs.File.Writer) {
    const sow = std.io.getStdErr().writer();
    const ebw = std.io.bufferedWriter(sow);
    return ebw;
}

pub fn reportErrorAndExit(comptime msg: []const u8, err: ?anyerror) noreturn {
    var ebw = bufferedStdErr();
    var writer = ebw.writer();

    if (err) |e| {
        writer.print("{s}: {!}\n", .{ msg, e }) catch unreachable;
    } else {
        writer.print("{s}\n", .{msg}) catch unreachable;
    }

    ebw.flush() catch unreachable;
    std.process.exit(if (err) |e| @intCast(@intFromError(e)) else 1);
}

pub fn printHelpAndExit(
    comptime clap: type,
    comptime params: []const clap.Param(clap.Help),
    comptime prog_name: []const u8,
) noreturn {
    var ebw = bufferedStdErr();
    var writer = ebw.writer();
    writer.writeAll(
        \\Usage:
        \\    
    ++ prog_name ++ " ") catch unreachable;
    clap.usage(writer, clap.Help, params) catch unreachable;
    writer.writeAll(
        \\
        \\
        \\Options:
        \\
    ) catch unreachable;
    clap.help(
        writer,
        clap.Help,
        params,
        .{},
    ) catch unreachable;
    ebw.flush() catch unreachable;
    std.process.exit(0);
}
