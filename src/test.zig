const is_wasm = @import("builtin").target.isWasm();

test {
    _ = @import("bech32.zig");

    _ = @import("format.zig");
    _ = @import("stream.zig");
    _ = @import("util.zig");
    if (is_wasm) {
        _ = @import("wage.zig");
    } else {
        _ = @import("cli.zig");
        _ = @import("keygen.zig");
        _ = @import("testkit.zig");
    }
    _ = @import("x25519.zig");
    _ = @import("zage.zig");
}
