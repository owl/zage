const std = @import("std");
const x25519 = @import("x25519.zig");
const zage = @import("zage.zig");

test "process test files" {
    if (@import("builtin").target.isWasm())
        return;
    var files_iter = (try std.fs.cwd().openDir("testdata", .{ .iterate = true })).iterate();
    var done: usize = 0;

    while (try files_iter.next()) |entry| {
        if (entry.kind != .file)
            continue;

        // Unsupported type
        if (std.mem.eql(u8, entry.name, "stanza_empty_body") or
            std.mem.eql(u8, entry.name, "stanza_empty_last_line") or
            std.mem.eql(u8, entry.name, "x25519_grease"))
            continue;

        // Unsupported recipient
        if (std.mem.eql(u8, entry.name, "stanza_valid_characters"))
            continue;

        // TODO: support scrypt/password
        if (std.mem.startsWith(u8, entry.name, "scrypt_"))
            continue;

        const file = try openTestFile(entry.name);

        { // hackity hack to skip non-X25519 for now
            const contents = try file.reader().readAllAlloc(testing.allocator, std.math.maxInt(u32));
            defer testing.allocator.free(contents);
            _ = std.mem.indexOf(u8, contents, "-> X25519 ") orelse continue;
            try file.seekTo(0);
        }

        done += 1;
        const md = try readTestFileMeta(file);

        if (md.armored == .yes)
            continue; // not implemented

        var out_buf: [0x100_000]u8 = undefined;
        var out_fbs = std.io.fixedBufferStream(&out_buf);

        //std.log.warn("trying {s} with\n{s}", .{ entry.name, md.identity });

        var arena = std.heap.ArenaAllocator.init(testing.allocator);
        defer arena.deinit();

        _ = zage.decrypt(arena.allocator(), md.identity, file, &out_fbs) catch |e| {
            if (md.expect == .success) {
                std.log.err("failed on file {d}: {s}\nwith {!}", .{
                    done,
                    entry.name,
                    e,
                });
                return;
            }
            //std.log.warn("expected {} on {s}\ngot {!}", .{ md.expect, entry.name, e });
            switch (md.expect) {
                .success => {
                    std.log.err("failed on file {d}: {s}\nwith {!}", .{
                        done,
                        entry.name,
                        e,
                    });
                    return;
                },
                .@"payload failure" => try testing.expect(e == error.InvalidData or
                    e == error.DecryptChunkFailed or
                    e == error.UnexpectedEof),
                // TODO: have a closer look at these
                .@"header failure" => try testing.expect(e == error.InvalidStanza or
                    e == error.InvalidMac or
                    e == error.InvalidType or
                    e == error.BadArgument or
                    e == error.UnsupportedRecipient or
                    e == error.UnexpectedEof or
                    e == error.NoMatchingIdentity or
                    e == error.InvalidPadding),
                .@"no match" => try testing.expect(e == error.NoMatchingIdentity),
                .@"HMAC failure" => try testing.expect(e == error.BadHeaderMac),
                .@"armor failure" => {},
            }
        };

        //try testing.expectEqual(@as(usize, 0), encrypted_file.rest.len);
    }
}

const Metadata = struct {
    pub const Key = enum {
        expect,
        payload,
        identity,
        passphrase,
        armored,
        @"file key",
        comment,
    };
    pub const Expect = enum {
        success,
        @"no match",
        @"HMAC failure",
        @"header failure",
        @"payload failure",
        @"armor failure",
    };
    pub const Armored = enum {
        no,
        yes,
    };

    expect: Expect,
    armored: Armored,
    identity: [zage.identity_bytes_len]u8,
};

fn readTestFileMeta(file: std.fs.File) !Metadata {
    var md: Metadata = undefined;
    var buf: [0x1_000]u8 = undefined;

    while (true) {
        const line = try file.reader().readUntilDelimiterOrEof(&buf, '\n');

        if (line) |l| {
            if (l.len == 0)
                return md;

            var col_it = std.mem.splitScalar(u8, l, ':');
            const first = col_it.next() orelse return md;

            const k = std.meta.stringToEnum(Metadata.Key, first) orelse {
                unreachable;
            };

            switch (k) {
                .expect => {
                    const next = col_it.next() orelse unreachable;
                    const v = std.meta.stringToEnum(Metadata.Expect, next[1..]);
                    md.expect = v orelse unreachable;
                },
                .armored => {
                    const next = col_it.next() orelse unreachable;
                    const v = std.meta.stringToEnum(Metadata.Armored, next[1..]);
                    md.armored = v orelse unreachable;
                },
                .identity => {
                    const next = col_it.next() orelse unreachable;
                    md.identity = next[1..][0..zage.identity_bytes_len].*;
                },
                else => {},
            }
        } else return md;
    }

    unreachable;
}

const testing = std.testing;

fn openTestFile(name: []const u8) !std.fs.File {
    const path = try std.fs.path.join(testing.allocator, &.{ "testdata", name });
    defer testing.allocator.free(path);
    const fil = try std.fs.cwd().openFile(path, .{});
    return fil;
}
