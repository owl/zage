// Wasm lib lives here.

const std = @import("std");
const zage = @import("zage.zig");
const util = @import("util.zig");
const cli = @import("cli.zig");
const x25519 = @import("x25519.zig");

extern "env" fn throwException(msg: [*]const u8, length: u32, code: u32) noreturn;
extern "env" fn consoleLog(msg: [*]const u8, length: u32) void;

/// Generate an identity, i.e. a key-pair.
/// `out` must be at least `ZAGE_IDENTITY_BYTES_LEN` long.
export fn generateIdentity(out: [*]u8) [*]const u8 {
    return @ptrCast(zage.generateIdentity(out[0..zage.identity_bytes_len]));
}

/// Generate a recipient from an identity, i.e. a public key from a secret key.
/// `out` must be at least `ZAGE_RECIPIENT_BYTES_LEN` long.
/// `identity` must be at least `ZAGE_IDENTITY_BYTES_LEN` long.
/// If an error occurs, a JS exception is thrown.
export fn recipientFromIdentity(
    out: [*]u8,
    identity: [*]const u8,
) [*]const u8 {
    const res = zage.recipientFromIdentity(
        @ptrCast(out),
        @ptrCast(identity),
    ) catch |e|
        reportErrorAndDie(e);

    return @ptrCast(res);
}

/// The minimum length for an identity buffer.
export fn getIdentityLength() u32 {
    return zage.identity_bytes_len;
}

/// The minimum length for a recipient buffer.
export fn getRecipientLength() u32 {
    return zage.recipient_bytes_len;
}

var gpa = std.heap.wasm_allocator;

export fn alloc(length: u32) [*]const u8 {
    const a = gpa.alloc(u8, length) catch |e|
        reportErrorAndDie(e);

    return a.ptr;
}

export fn free(ptr: [*]u8, length: u32) void {
    gpa.free(@as([]u8, ptr[0..length]));
}

/// Error codes.
const RECIPIENT_FROM_IDENTITY_ERROR = util.EnumFromErrorSet(
    zage.RecipientFromIdentityError,
    zage.RecipientFromIdentityError.InvalidIdentity,
);

/// Decrypt the ciphertext in `input`.
/// The `output` must be long enough to fit the plaintext.
/// If an error occurs, a JS exception is thrown.
export fn decrypt(
    identity: [*]const u8,
    input: [*]const u8,
    input_length: u32,
    output: [*]u8,
    output_length: u32,
) u32 {
    var input_fbs = std.io.fixedBufferStream(input[0..input_length]);
    var output_fbs = std.io.fixedBufferStream(output[0..output_length]);

    const n = zage.decrypt(
        gpa,
        identity[0..74].*,
        &input_fbs,
        &output_fbs,
    ) catch |e|
        reportErrorAndDie(e);

    return @intCast(n);
}

/// Encrypt the plaintext in `input`.
/// The `output` must be long enough to fit the ciphertext.
/// If an error occurs, a JS exception is thrown.
export fn encrypt(
    // `recipients_length` × `getRecipientLength()` bytes
    recipients: [*]const [*]const u8,
    recipients_length: u32,
    input: [*]const u8,
    input_length: u32,
    output: [*]u8,
    output_length: u32,
) void {
    var input_fbs = std.io.fixedBufferStream(input[0..input_length]);
    var output_fbs = std.io.fixedBufferStream(output[0..output_length]);

    var recips = zage.RecipientSet.init(std.heap.wasm_allocator);
    defer recips.deinit();

    for (recipients[0..recipients_length]) |recip| {
        const r = recip[0..zage.recipient_bytes_len].*;
        const xr = x25519.Recipient.fromAgeRecipient(r) catch |e|
            reportErrorAndDie(e);

        recips.append(xr) catch |e|
            reportErrorAndDie(e);
    }

    zage.encrypt(
        recips,
        &input_fbs,
        &output_fbs,
    ) catch |e| {
        const en = @errorName(e);
        throwException(@ptrCast(en), @intCast(en.len), @intFromError(e));
    };
}

fn reportErrorAndDie(e: anyerror) noreturn {
    const en = @errorName(e);
    throwException(@ptrCast(en), @intCast(en.len), @intFromError(e));
}
