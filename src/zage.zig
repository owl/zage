const std = @import("std");
const builtin = @import("builtin");
const bech32 = @import("bech32.zig");
const crypto = std.crypto;
const rand = std.rand;
const X25519 = crypto.dh.X25519;
const testing = std.testing;

pub const secret_key_prefix = "AGE-SECRET-KEY-";
pub const secret_key_version = "1";
pub const public_key_prefix = "age";

pub const identity_bytes_len = bufLen(secret_key_prefix.len, bech32.calcExpansion(X25519.secret_length));
pub const recipient_bytes_len = bufLen(public_key_prefix.len, bech32.calcExpansion(X25519.public_length));

extern "env" fn wasmRandomBytes(ptr: [*]u8, len: u32) void;

fn bufLen(comptime prefix_len: comptime_int, comptime expansion_len: comptime_int) comptime_int {
    return prefix_len + 1 + expansion_len + 6;
}

fn hostRandomBytes(buffer: []u8) void {
    if (builtin.target.isWasm())
        wasmRandomBytes(@ptrCast(buffer), buffer.len)
    else
        crypto.random.bytes(buffer);
}

pub const AgeIdentity = [identity_bytes_len]u8;
pub const AgeRecipient = [recipient_bytes_len]u8;
pub const EncryptedFileKey = [32]u8;
pub const FileKey = [16]u8;
pub const Salt = [64]u8;
pub const Hmac = [32]u8;
pub const Nonce = [16]u8;
pub const StreamKey = [HmacSha256.mac_length]u8;

pub fn generateIdentity(out: *AgeIdentity) []const u8 {
    var scalar: x25519.SecretKey = undefined;

    hostRandomBytes(&scalar);

    return bech32.standard_uppercase.Encoder.encode(
        out,
        secret_key_prefix,
        &scalar,
        .bech32,
    );
}

test "generate identity" {
    var buf: AgeIdentity = undefined;
    const id = generateIdentity(&buf);
    try testing.expectStringStartsWith(id, secret_key_prefix ++ secret_key_version);
    try testing.expectEqual(@as(usize, identity_bytes_len), id.len);
}

pub const RecipientFromIdentityError = error{
    InvalidIdentity,
    InvalidPrefix,
    InvalidEncoding,
    InvalidLength,
} || bech32.Error || crypto.errors.IdentityElementError;

pub fn recipientFromIdentity(
    out: *AgeRecipient,
    identity: *const AgeIdentity,
) RecipientFromIdentityError![]const u8 {
    var buf: AgeRecipient = undefined;
    const res = try bech32.standard_uppercase.Decoder.decode(&buf, identity);
    const mem = std.mem;

    if (!mem.startsWith(u8, identity, secret_key_prefix ++ secret_key_version))
        return RecipientFromIdentityError.InvalidIdentity;
    if (!mem.eql(u8, res.hrp, secret_key_prefix))
        return RecipientFromIdentityError.InvalidPrefix;
    if (res.encoding != .bech32)
        return RecipientFromIdentityError.InvalidEncoding;
    if (res.data.len != X25519.secret_length)
        return RecipientFromIdentityError.InvalidLength;

    const pk = res.data[0..X25519.secret_length].*;
    const recipient = try X25519.recoverPublicKey(pk);

    return bech32.standard.Encoder.encode(
        out,
        public_key_prefix,
        &recipient,
        .bech32,
    );
}

test "generate recipient from identity" {
    var buf: AgeIdentity = undefined;
    var id = generateIdentity(&buf);
    const recip = try recipientFromIdentity(buf[0..recipient_bytes_len], id[0..identity_bytes_len]);
    try testing.expectStringStartsWith(recip, public_key_prefix ++ "1");
}

test "matches official age recipient generation" {
    var buf: AgeRecipient = undefined;
    const id = "AGE-SECRET-KEY-1TDEXPG0T2E3UEQTCVTNJ4P9ZTTD3S60HQYETJF24FJJ2WAQX45GS4YJ49N";
    const recip = try recipientFromIdentity(&buf, id);
    try testing.expectEqualStrings(recip, "age10r3a7s6jd0gzmrh7t2ajkn0zsp8ndhqxd7hm7aj76u93rnh55amqhfxe7r");
}

const format = @import("format.zig");
const util = @import("util.zig");
const stream = @import("stream.zig");
const x25519 = @import("x25519.zig");

const ChaCha = std.crypto.aead.chacha_poly.ChaCha20Poly1305;
const Hkdf = std.crypto.kdf.hkdf.HkdfSha256;
const HmacSha256 = std.crypto.auth.hmac.sha2.HmacSha256;

pub const EncryptError = error{
    BadRecipient,
};

pub const RecipientSet = std.ArrayList(x25519.Recipient);

pub fn encrypt(
    recipients: RecipientSet,
    input_file: anytype,
    output_file: anytype,
) EncryptError!void {
    _ = recipients; // autofix
    var buffered_output_file = std.io.bufferedWriter(output_file.writer());
    defer buffered_output_file.flush() catch unreachable;
    const output_writer = buffered_output_file.writer();
    _ = output_writer; // autofix
    var buffered_input_file = std.io.bufferedReader(input_file.reader());
    const input_reader = buffered_input_file.reader();
    _ = input_reader; // autofix

    var file_key: FileKey = undefined;
    hostRandomBytes(&file_key);
}

pub const DecryptError = error{
    UnsupportedRecipient,
    NoMatchingIdentity,
    BadHeaderMac,
    UnexpectedEof,
} ||
    x25519.Identity.FromSecretError ||
    x25519.Identity.UnwrapError;

pub fn decrypt(
    allocator: std.mem.Allocator,
    identity: AgeIdentity,
    input_file: anytype,
    output_file: anytype,
) !usize {
    var buffered_input_file = std.io.bufferedReader(input_file.reader());
    var input_reader = buffered_input_file.reader();

    var chunk_buf: format.EncodedChunk = undefined;
    defer std.crypto.utils.secureZero(u8, &chunk_buf);
    var hr = format.headerReader(input_reader, &chunk_buf);

    _ = try hr.readVersion();

    const Recipient = struct { []const u8, []const u8 };
    var stanzae = std.ArrayList(Recipient).init(allocator);

    // TODO: What's a reasonable upper bound for recipients?
    // Eventually it will OOM.
    const hm = blk: for (0..std.math.maxInt(u8)) |i| {
        if (0 == i) {
            _ = try hr.readStanza();
        } else {
            const maybe_mac = try hr.readStanzaOrMac();

            if (maybe_mac.kind == .mac)
                break :blk maybe_mac;
        }

        _ = try hr.readType();

        const eph_arg = try hr.readArgument(.last);
        const bod = try hr.readBodyLine();

        try stanzae.append(Recipient{
            eph_arg.sourceSlice(&chunk_buf),
            bod.sourceSlice(&chunk_buf),
        });
    } else return DecryptError.UnexpectedEof;

    // All of the header up until `--- `, excluding the space.
    const mac_slice = chunk_buf[0 .. hm.offset - 1];
    const xid = try x25519.Identity.fromAgeIdentity(identity);

    const b64 = std.base64;
    var b64_dec = b64.Base64Decoder.init(b64.standard_alphabet_chars, null);

    for (stanzae.items) |recipient| {
        const file_key = xid.unwrap(
            recipient[0],
            recipient[1],
        ) catch
            continue orelse
            continue;

        const mac = headerMacSlice(file_key, mac_slice);

        var hmac: Hmac = undefined;
        try b64_dec.decode(&hmac, hm.sourceSlice(&chunk_buf));

        if (!std.crypto.utils.timingSafeEql(Hmac, mac, hmac))
            return DecryptError.BadHeaderMac;

        var nonce: Nonce = undefined;
        _ = try input_reader.read(&nonce);

        const stream_key = streamKey(file_key, nonce);
        var chunkus = stream.chunkyReader(buffered_input_file, ChaCha, stream_key);

        var buffered_output_file = std.io.bufferedWriter(output_file.writer());
        defer buffered_output_file.flush() catch unreachable;
        var output_writer = buffered_output_file.writer();
        var written: usize = 0;

        while (true) {
            const pos = buffered_input_file.start;
            const dn = chunkus.read(&chunk_buf) catch |e| switch (e) {
                error.MaybeDecryptChunkFailed => blk: {
                    // Take that and rewind it back.
                    buffered_input_file.start = pos;
                    break :blk try chunkus.read(&chunk_buf);
                },
                else => return e,
            };

            if (dn == 0)
                break;

            written += dn;

            output_writer.writeAll(chunk_buf[0..dn]) catch unreachable;
        }

        if (!chunkus.nonce.isLast())
            return DecryptError.UnexpectedEof;

        return written;
    }

    return DecryptError.NoMatchingIdentity;
}

fn streamKey(file_key: FileKey, nonce: Nonce) [ChaCha.key_length]u8 {
    var stream_key: StreamKey = undefined;
    const prk = Hkdf.extract(&nonce, &file_key);
    Hkdf.expand(&stream_key, "payload", prk);
    return stream_key;
}

pub fn headerMacSlice(
    file_key: FileKey,
    header_slice: []const u8,
) [HmacSha256.mac_length]u8 {
    var hmac_key: [HmacSha256.mac_length]u8 = undefined;
    var null_salt = [0]u8{};
    const prk = Hkdf.extract(&null_salt, &file_key);
    Hkdf.expand(&hmac_key, "header", prk);
    var hh = HmacSha256.init(&hmac_key);
    hh.update(header_slice);
    var hmac: [HmacSha256.mac_length]u8 = undefined;
    hh.final(&hmac);
    return hmac;
}
