const std = @import("std");
const format = @import("format.zig");

const StreamError = error{
    DecryptChunkFailed,
    MaybeDecryptChunkFailed,
    UnexpectedEof,
};

pub fn ChunkyReader(comptime ReaderType: type, comptime AeadType: type) type {
    return struct {
        child_reader: ReaderType,
        stream_key: [Aead.key_length]u8,
        nonce: Nonce = std.mem.zeroes(Nonce),

        pub const Aead = AeadType;
        pub const Error = StreamError || Nonce.Error || ReaderType.Error;
        pub const Reader = std.io.Reader(*@This(), Error, read);

        const Self = @This();
        const chunk_length = format.chunk_length;
        const encoded_chunk_length = chunk_length + Aead.tag_length;

        /// Reads encrypted chunks into a chunk-sized buffer.
        pub fn read(self: *Self, out: []u8) Error!usize {
            const n = try self.child_reader.read(out);

            if (0 == n)
                return if (!self.nonce.isLast())
                    StreamError.UnexpectedEof
                else
                    0;

            // The case where the final chunk is equal to `encoded_chunk_length`
            // is handled higher up.
            const last = n < encoded_chunk_length;

            return self.decryptChunk(out, out[0..n], last);
        }

        pub fn decryptChunk(
            self: *Self,
            out: []u8,
            chunk: []const u8,
            last: bool,
        ) Error!usize {
            try self.nonce.setLast(last);

            const de = aeadDecrypt(
                out,
                chunk,
                self.nonce.toBytes(),
                self.stream_key,
            ) catch
                return if (last)
                // zig fmt: off
                    StreamError.DecryptChunkFailed
                else blk: {
                    try self.nonce.setLast(true);
                    break :blk StreamError.MaybeDecryptChunkFailed;
                };
                // zig fmt: on

            try self.nonce.incrementCounter();

            return de.len;
        }

        pub fn reader(self: Self) Reader {
            return .{ .context = self };
        }
    };
}

pub fn aeadDecrypt(
    out: []u8,
    ciphertext: []const u8,
    nonce: format.Nonce,
    secret_key: format.Key,
) std.crypto.errors.AuthenticationError![]const u8 {
    const tag_length = @typeInfo(format.Tag).Array.len;
    if (ciphertext.len > (1 << 38) - 48)
        return std.crypto.errors.AuthenticationError.AuthenticationFailed;

    if (ciphertext.len < tag_length)
        return std.crypto.errors.AuthenticationError.AuthenticationFailed;

    const tag_pos = ciphertext.len - tag_length;
    const msg = ciphertext[0..tag_pos];
    const tag: format.Tag = ciphertext[tag_pos..][0..tag_length].*;

    try std.crypto.aead.chacha_poly.ChaCha20Poly1305.decrypt(
        out[0..tag_pos],
        msg,
        tag,
        "",
        nonce,
        secret_key,
    );

    return out[0..tag_pos];
}

pub fn aeadEncrypt(
    out: []u8,
    plaintext: []const u8,
    key: [format.Key]u8,
) std.crypto.errors.AuthenticationError![]const u8 {
    if ((1 << 38) - 64 < plaintext.len)
        return std.crypto.errors.AuthenticationError.AuthenticationFailed;

    const tag_pos = plaintext.len -| format.Tag;
    const msg = plaintext[0..tag_pos];
    const tag: format.Tag = plaintext[tag_pos..][0..@sizeOf(format.Tag)].*;

    // Each key is used only once.
    const null_nonce = [_]u8{0x00} ** @typeInfo(format.Nonce).Array.len;

    try std.crypto.aead.chacha_poly.ChaCha20Poly1305.encrypt(
        out[0..tag_pos],
        msg,
        tag,
        "",
        null_nonce,
        key,
    );

    return out[0..tag_pos];
}

pub fn chunkyReader(
    child_reader: anytype,
    comptime Aead: type,
    stream_key: [Aead.key_length]u8,
) ChunkyReader(@TypeOf(child_reader), Aead) {
    return .{
        .child_reader = child_reader,
        .stream_key = stream_key,
    };
}

/// Adapted from rage, thanks pals.
pub const Nonce = struct {
    data: u128 = 0,

    pub const Error = error{ Overflow, InvalidData };
    pub const Self = @This();

    pub fn setCounter(self: *Self, val: u64) void {
        self.data = @as(u128, @intCast(val)) << 8;
    }

    pub fn incrementCounter(self: *Self) !void {
        self.data += 1 << 8;

        if (self.data >> (8 * @sizeOf(format.Nonce)) != 0)
            return Error.Overflow;
    }

    pub fn isLast(self: Self) bool {
        return self.data & 1 != 0;
    }

    pub fn setLast(self: *Self, last: bool) !void {
        if (self.isLast())
            return Error.InvalidData;

        self.data |= @as(u128, @intFromBool(last));
    }

    pub fn toBytes(self: Self) format.Nonce {
        var buf: [@sizeOf(u128)]u8 = undefined;
        var fbs = std.io.fixedBufferStream(&buf);
        fbs.writer().writeInt(u128, self.data, .big) catch unreachable;
        return buf[4..].*;
    }
};

test "nonce" {
    var n = Nonce{};
    try std.testing.expect(!n.isLast());
    try n.incrementCounter();
    try std.testing.expect(!n.isLast());
    try n.setLast(true);
    try std.testing.expect(n.isLast());

    const r = n.setLast(true);
    try std.testing.expectError(error.InvalidData, r);
}
