const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});
    const strip = b.option(bool, "strip", "Strip the artifacts") orelse (optimize != .Debug);

    const deps = .{
        "clap",
        "zig-datetime",
    };

    const lib_mod = b.addModule(
        "zage",
        .{
            .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "zage.zig"),
        },
    );

    // CLI exe
    // `zage-keygen` is a symlink to `zage`
    {
        const cli_exe = b.addExecutable(.{
            .name = "zage",
            .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "cli.zig"),
            .target = target,
            .optimize = optimize,
            .single_threaded = true,
            .strip = strip,
        });

        inline for (deps) |name| {
            const dep = b.dependency(name, .{});
            cli_exe.root_module.addImport(name, dep.module(name));
        }

        cli_exe.link_function_sections = true;
        cli_exe.link_data_sections = true;
        cli_exe.link_gc_sections = true;

        cli_exe.pie = b.option(
            bool,
            "pie",
            "Enable Position Independent Executable (PIE)",
        ) orelse switch (target.result.os.tag) {
            .macos, .openbsd => true,
            else => false,
        };

        const install_exe = b.addInstallArtifact(cli_exe, .{});
        b.getInstallStep().dependOn(&install_exe.step);

        const run_cmd = b.addRunArtifact(cli_exe);

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("run", "Run zage");

        run_step.dependOn(&run_cmd.step);

        const symlinks = @import("build/symlinks.zig");
        const symlinker = symlinks.getSymlinkerFromFilesystemTest(
            b,
            .prefix,
            "rootfs.metadta",
        ) catch unreachable;

        const symlink_step = symlinker.createInstallSymlinkStep(
            b,
            "zage",
            std.Build.InstallDir{ .bin = {} },
            "zage-keygen",
        );

        install_exe.step.dependOn(symlink_step);
    }

    // wasm lib
    const wasm_lib = b.addExecutable(.{
        .name = "wage",
        .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "wage.zig"),
        .strip = strip,
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
    });
    const install_wasm = b.addInstallArtifact(wasm_lib, .{});

    {
        wasm_lib.entry = .disabled;
        wasm_lib.root_module.export_symbol_names = &[_][]const u8{
            "getIdentityLength",
            "getRecipientLength",
            "encrypt",
            "decrypt",
            "generateIdentity",
            "recipientFromIdentity",
        };
        // <https://github.com/ziglang/zig/issues/8633>
        //wasm_lib.global_base = 6560;
        wasm_lib.rdynamic = true;
        wasm_lib.import_memory = true;
        wasm_lib.stack_size = 1024 * 1024;

        // must match wage.js
        wasm_lib.initial_memory = 32 * std.wasm.page_size;
        // wasm_lib.max_memory = 8 * std.wasm.page_size;

        wasm_lib.root_module.addImport("zage", lib_mod);
        // wasm_lib.root_module.export_symbol_names = &[_][]const u8{
        //     "getIdentityLength",
        //     "getRecipientLength",
        //     "generateIdentity",
        //     "recipientFromIdentity",
        //     "encrypt",
        //     "decrypt",
        // };

        const build_step = b.step("wasm", "Build Wasm library");

        build_step.dependOn(&install_wasm.step);
    }

    // unit tests
    {
        const test_step = b.step("test", "Run unit tests");
        const unit_tests = b.addTest(.{
            .root_source_file = b.path("src" ++ std.fs.path.sep_str ++ "test.zig"),
            .target = target,
            .optimize = optimize,
        });

        inline for (deps) |name| {
            const dep = b.dependency(name, .{});
            const mod = dep.module(name);
            unit_tests.root_module.addImport(name, mod);
        }

        const run_unit_tests = b.addRunArtifact(unit_tests);

        test_step.dependOn(&run_unit_tests.step);

        if (target.result.isWasm())
            test_step.dependOn(&install_wasm.step);
    }
}
