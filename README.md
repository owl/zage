# zage
Another implementation of [the age encryption tool](https://age-encryption.org/).

***zage is not well-tested or mature, you probably don't want to use it.***

## Why?
I wanted to learn how it works,
and I thought a small implementation could be useful for Wasm/web.

`zage` + `zage-keygen` is 69 KiB currently (static, linux amd64),
and the Wasm library is 15 KiB gzipped over HTTP.

## Status/todo
- `zage-keygen`
    - [x] Identity generation
    - [x] Recipient from identity generation
    - [x] Convert identity file to recipient file
- `zage`
    - [x] Parse the age file format
    - [x] Accept/reject all testkit age files
    - [x] Support X25519
    - [ ] Support scrypt/passphrases
    - [x] File decryption
    - [ ] File encryption
        - [ ] AEAD encrypt
        - [ ] Write out
    - [ ] PEM/"armor" support
- `wage` (Wasm library)
    - [x] Wasm interface to main zage functions
    - [x] Example Wasm program
- General
    - [ ] Define some nice data types and use those consistently.
        - [ ] Add interfaces/vtable for recipients, so that scrypt can be added more easily.
    - [x] Replace mecha with streaming parser to reduce allocations.
    - [ ] Translations?

## Building, testing, running
The standard Zig process:

```bash
# build the default target
zig build

# run all tests
zig build test

# install locally
zig build install -Doptimize=ReleaseSafe --prefix ~/.local

# run
zage -di <identity file> <encrypted file>
zage-keygen

# run without installing
zig build run -- -di <identity file> <encrypted file>
```

Use `zig build --help` to see all the options.
