"use strict";

const PAGE_SIZE = 0x1_00_00;
const INITIAL_PAGES = 32;

const memory = new WebAssembly.Memory({
  // must match build.zig
  initial: INITIAL_PAGES,
});

// Holding on to `Uint8Array`s doesn't work; they get invalidated when the buffer grows.
class Allocation {
  /** @property {number} */
  pointer;
  /** @property {number} */
  length;

  /**
   * @param {number} ptr
   * @param {number} length
   */
  constructor(ptr, length) {
    this.pointer = ptr;
    this.length = length;
    Object.freeze(this);
  }

  /**
   * For convenience, not guaranteed to remain valid.
   * @returns {Uint8Array}
   */
  asUint8Array() {
    const arr = new Uint8Array(memory.buffer, this.pointer, this.length);
    return arr;
  }

  /**
   * Copy a slice of the underlying buffer to an array.
   * @returns {Uint8Array}
   */
  copyToUint8Array() {
    const slice = memory.buffer.slice(this.pointer, this.pointer + this.length);
    const arr = new Uint8Array(slice);
    return arr;
  }
}

const main = async () => {
  const imports = {
    env: {
      memory,
      __indirect_function_table: new WebAssembly.Table({ element: "anyfunc", initial: 400 }),
      // How does this work? No idea.
      // I think stack grows downward, so i set this equal to the stack size in build.zig
      __stack_pointer: new WebAssembly.Global({ value: "i32", mutable: true }, 1024 * 1024),
      __memory_base: new WebAssembly.Global({ value: "i32", mutable: false }, 0),
      __table_base: new WebAssembly.Global({ value: "i32", mutable: false }, 0),
      consoleLog: (ptr, len) => {
        const s = new Uint8Array(memory.buffer, ptr, len);
        const msg = new TextDecoder().decode(s);
        console.log(msg);
      },
      wasmRandomBytes: (ptr, len) => {
        const arr = new Uint8Array(memory.buffer, ptr, len);
        crypto.getRandomValues(arr);
      },
      throwException: (ptr, len, code) => {
        const s = new Uint8Array(memory.buffer, ptr, len);
        const msg = new TextDecoder().decode(s);
        alert(`oh no: ${msg} (${code})`);
        throw new Error(`${msg} (${code})`);
      },
    }
  };

  const mod = await WebAssembly.compileStreaming(fetch("../zig-out/bin/wage.wasm"));
  const { exports } = await WebAssembly.instantiate(mod, imports);

  /**
   * @param {number} length
   * @returns {Allocation}
   */
  const alloc = (length) => {
    const ptr = exports.alloc(length);
    return new Allocation(ptr, length);
  };

  /**
   * @param {Allocation} allo
   * @returns {void}
   */
  const free = (allo) => {
    exports.free(allo.pointer, allo.length);
  };

  const mainEl = document.getElementById("keygen");
  const output = document.getElementById("identity");
  const btn = document.createElement("button");

  btn.innerText = "Generate identity";
  mainEl.insertBefore(btn, output);

  btn.addEventListener("click", _ => {
    const idBuf = alloc(exports.getIdentityLength());
    const recipBuf = alloc(exports.getRecipientLength());

    try {
      exports.generateIdentity(idBuf.pointer);
      exports.recipientFromIdentity(
        recipBuf.pointer,
        idBuf.pointer,
      );

      const recipStr = stringFromAllocation(recipBuf);
      const dateStr = (new Date()).toISOString().slice(0, -5);
      const pre = document.createElement("pre");

      pre.innerText = `# created: ${dateStr}+00:00\
        \n# public key: ${recipStr}\
        \n${stringFromAllocation(idBuf)}
        `;

      const saveLink = makeSaveLink(pre.innerText);

      output.replaceChildren();
      output.appendChild(pre);
      output.appendChild(saveLink);
    } catch (e) {
      alert(`oh no: ${e}`);
      console.log(e);
    } finally {
      free(recipBuf);
      free(idBuf);
    }
  });

  document.getElementById("dbtn").addEventListener("click", _ => {
    const testMsg = atob(document.getElementById("msg").value);
    const id = alloc(exports.getIdentityLength());
    const msg = alloc(testMsg.length);
    const out = alloc(10);

    try {
      const testId = document.getElementById("test-id").value;
      new TextEncoder().encodeInto(testId, id.asUint8Array());

      const msgArr = new Uint8Array(testMsg.split('').map(b => b.charCodeAt(0)));
      msg.asUint8Array().set(msgArr);

      console.log(id.pointer, msg.pointer, out.pointer, memory.buffer.byteLength)
      const writ = exports.decrypt(id.pointer, msg.pointer, msg.length, out.pointer, out.length);

      const outputStr = new TextDecoder().decode(memory.buffer.slice(out.pointer, out.pointer + writ));
      console.assert(outputStr == "age");
      alert(`decrypted message: '${outputStr}'`);
    } catch (e) {
      alert(`oh no: ${e}`);
      console.log(e);
    } finally {
      free(out);
      free(msg);
      free(id);
    }
  });
};


main();

/**
 * @param {Allocation} allo
 * @returns {string}
 */
const stringFromAllocation = (allo) => {
  const slice = allo.asUint8Array();
  const decoder = new TextDecoder();
  return decoder.decode(slice);
};

/**
 * @param {string} text Text to download.
 * @returns {HTMLAnchorElement}
 */
const makeSaveLink = (text) => {
  const textAsBlob = new Blob([text], { type: "text/plain", endings: "native" });
  const fileNameToSaveAs = "secret-key.txt";
  const downloadLink = document.createElement("a");
  downloadLink.download = fileNameToSaveAs;
  downloadLink.appendChild(document.createTextNode("Save identity"));
  downloadLink.href = window.URL.createObjectURL(textAsBlob);

  return downloadLink;
};
